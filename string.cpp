#include "string.h"

StringValue::StringValue(char* str) {
  string = str;
}

void StringValue::deref() {
  --ref;
}

void StringValue::refer() {
  ++ref;
}

StringValue::~StringValue() {
  delete[] string;
}

MyString::MyString(char const *str) {
  char * s = new char[strlen(str) +1];
  strcpy(s, str);
  string_ = new StringValue{s};
}

MyString::MyString( MyString const &rvl) {
  string_ = rvl.string_;
  string_->refer();
}

MyString::MyString( MyString&& rvl) {
  string_=rvl.string_;
  rvl.string_ = nullptr;
}

char const *MyString::get_str() const {
  if (string_ == nullptr) {
    return nullptr;
  }
  return string_->get_str();
}

void MyString::deref() {
  if ( string_ != nullptr ) {
    string_->deref();
    if ( string_->get_ref() == 0 ) {
      delete string_;
    }
    string_=nullptr;
  }
}

void MyString::ref(StringValue* str) {
  string_=str;
  if ( string_ !=  nullptr) {
    string_->refer();
  }
}

MyString& MyString::operator=(MyString  const &rvl) {
  if ( string_ != rvl.string_) {
    deref();
    ref(rvl.string_);
  }
  return *this;
}

MyString&  MyString::operator=(char const *str) {
  deref();
  char *s = new char[strlen(str)+1];
  strcpy(s, str);  
  string_ = new StringValue{s};
  return *this;
}

MyString& MyString::operator=(MyString &&rvl) {
  deref();
  string_= rvl.string_;
  rvl.string_ = nullptr;
  return *this;
}

char& MyString::operator[](size_t s) {
  if (s > size() ) {
    throw "operator[]";
  }
  if (string_->get_ref() == 1 ) {
    return *(string_->get_str()+s);
  }
  else {
    StringValue* tmp = string_;
    char *str_tmp = new char[size()+1];
    strcpy(str_tmp, tmp->get_str());
    string_ = new StringValue{str_tmp};
    tmp->deref();
    return *(string_->get_str()+s);
  }
}

MyString operator+(MyString const &lvl, MyString const &rvl) {
  size_t len  = lvl.size() + rvl.size();
  char *s = new char[len+1];
  if ( lvl.size() != 0 ) {
    strcpy(s, lvl.get_str());
  }
  if ( rvl.size() != 0 ) {
    strcpy(s+lvl.size(), rvl.get_str());
  }
  return MyString{s} ;
}

MyString& MyString::operator+=(MyString const &rvl) {
  MyString b = *this + rvl;
  string_ = b.string_;
  b.string_ = nullptr;
  return *this;
}

MyString operator+(MyString const &str, const char c) {
  int len  = str.size() + 1;
  char *s = new char[len+1];
  if ( str.size() != 0 ) {
    strcpy(s, str.get_str());
  }
  s[str.size()] = c;
  s[len]='\0';
  return MyString{s};
}

MyString& MyString::operator+=(const char c) {
  MyString b = *this + c;
  string_ = b.string_;
  b.string_=nullptr;
  return *this;
}

std::ostream& operator<<(std::ostream& os, MyString const &s) {
  if (s.get_str() == nullptr) {
    return os;
  }
  return os <<  s.get_str();
}
